//Metodos de cadenas
//concat()
/*
let cadena = "cadena de prueba";
let resultado = cadena.concat(" Hola");
let cadena2 = new String("Cadena dos");

document.write(resultado);
resultado = cadena.concat(cadena2);
document.write(`<br>`);
document.write(resultado);*/
//

let cadena = "¿comienza con? ¿incluye a? ¿termina en? ¿termina en?";
let cadena1 = "¿comienza";
let cadena2 = "incluye a";
let cadena3 = "¿termina en?";

document.write(`
la cadena: "cadena" comienza con "${cadena1}"
${cadena.startsWith(cadena1)}<br>
la cadena: "cadena" comienza con "${cadena2}"
${cadena.startsWith(cadena2)}<br>
la cadena: "cadena" comienza con "${cadena3}"
${cadena.startsWith(cadena3)}<br><br>`);

document.write(`
la cadena: "cadena" incluye a "${cadena1}"
${cadena.includes(cadena1)}<br>
la cadena: "cadena" incluye a "${cadena2}"
${cadena.includes(cadena2)}<br>
la cadena: "cadena" incluye a "${cadena2}"
${cadena.includes(cadena3)}<br><br>`);

document.write(`
la cadena: "cadena" termina en "${cadena1}"
${cadena.endsWith(cadena1)}<br>
la cadena: "cadena" termina en "${cadena2}"
${cadena.endsWith(cadena2)}<br>
la cadena: "cadena" termina en "${cadena3}"
${cadena.endsWith(cadena3)}<br><br>`);

document.write(`
	El indice de el comienzo de la palabra "incluye" es: 
	${cadena.indexOf("incluye")}<br>
	El indice de el comienzo de la palabra "comienza" es: 
	${cadena.indexOf("comienza")}<br>
	El ultimo indice de la cadena "cadena" es: 
	${cadena.lastIndexOf("termina")}<br>
	El primer indice de la cadena "cadena" es: 
	${cadena.indexOf("?")}<br>
	${cadena[1]} ${cadena[2]} ${cadena[3]} 
	${cadena[4]} ${cadena[5]} ${cadena[6]} 
	${cadena[7]} ${cadena[8]} ${cadena[9]}`);