/*
//While

let contador = 0;

while (contador < 10){
	contador++;
	document.write(`
		<p>${contador}</p>

		`)
}

///Do While
let contador2 = 10;

do {
	contador2--;
document.write(`
		<p>${contador2}</p>

	`)
	
}
while (contador2 > 0);*/

/// Break
/*
let numero = 0;

while(numero < 100){
	numero++;
	document.write(`
			<p>${numero}</p>`
		);
	if (numero == 40){
		break;
	}
}*/

// for y continue
/*
let i = 30;

for (let i = 10; i >= 0 ; i--){
	
	if(i == 4){
		continue;
	}

	document.write(`
		Vuelta numero ${i} <br>
		`)
	
}	

document.write(i);*/
//For in y For of
/*
let animales = ["perro", "gato", "gallo", "oveja"];
animales.edad = 20;

for(animal in animales){
	document.write((parseInt(animal)+1) + "<br>");
}
for (animal of animales){

	document.write(animal + "<br>");
}

//Propiedad de un array
document.write(animales.edad);*/

// Sentencia Label
/*
let array1 = ["maria", "josefa", "roberta"];
let array2 = ["pedro", "marcelo", array1, "josefina"];

forPrincipal:
for(let array in array2){
	if (array == 2){
		for (let array of array1){
			continue forPrincipal;
		document.write(array + "<br>");
			

		}
	} else {
		document.write(array2[array] + "<br>");
	}

}*/

//Funciones


