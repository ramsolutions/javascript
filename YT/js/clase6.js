//frutas = ["manzana", "coco", "kiwi", "pera", "mango"];

//document.write(frutas[1]);

/*frutas = [
citricas ["limon", "mandarina", "naranja", "toronja"],
otros    ["pera" ,  "manzan"  , "patilla", "melon"]
]*/


// ----------- Arrays Asociativos u Objetos ------------------- //

let pc = {
	nombre: "Rene Pc",
	procesador: "Intel Celeron",
	ram: "4GB",
	espacio: "256GB"
};



//document.write(pc["nombre"]);

document.write(`
		<h1>Descripcion de mi pc</h1>
		<p>Su nombre es ${pc["nombre"]}, su procesador es: 
${pc["procesador"]}, tiene una ram de ${pc["ram"]} <br>y tiene de espacio: 
${pc["espacio"]}
		</p>
	`);
