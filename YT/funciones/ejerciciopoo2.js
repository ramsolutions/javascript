class App{
    constructor(descargas, puntuacion, peso){
        this.descargas = descargas;
        this.puntuacion = puntuacion;
        this.peso = peso;
        this.iniciada = false;
        this.instalada = false;
    }

    abrir(){
        if (this.instalada && !this.iniciada){
            alert("Iniciando app");
            this.iniciada = true;
            alert("app iniciada");
        }else if(!this.instalada){
            alert("la app no esta instalada pruebe instalandola primero");
        }
        else if(this.instalada && this.iniciada){
            alert("la app ya se esta ejecutando en una instancia");
        }
    }

    cerrar(){
        if (this.instalada && this.iniciada){
            alert("Cerrando app");
            this.iniciada = false;
            alert("app cerrada");
        } else if(!this.instalada){
            alert("la app no esta instalada");
        } else if(this.instalada && !this.iniciada){
            alert ("la app no está iniciada");
        }
    }

    instalar(){
        if(!this.instalada){
            alert("descargando app");
            alert("instalando app");
            this.instalada = true;
            alert("app instalada");
        } else if(this.instalada){
            alert("la app ya estaba instalada");
        }
    }
    desinstalar(){
        if(this.instalada){
            alert("desinstalando app");
            this.instalada = false;
            alert("app desinstalada");
        } if(!this.instalada){
            alert("la app no estaba instalada");
        }
    }
    toString(){
        return `
            Informacion de la App<br>
        Descargas: ${this.descargas}<br>
        Puntuacion: ${this.puntuacion}<br>
        Peso:       ${this.peso}<br>

        `;
    }
}

const app = new App("2k, 4.6 estrellas, 120Mb"); 
document.write(`la app esta instalada?${app.instalada}<br>`);
document.write(`<br>`);
document.write(`la app esta instalada?${app.iniciada}<br>`);
document.write(`<br>`);
document.write(app.toString());
document.write(`<br>`);
app.instalar();
document.write(`<br>`);
app.abrir();
document.write(`<br>`);
document.write(`la app esta instalada?${app.instalada}<br>`);
document.write(`<br>`);
document.write(`la app esta instalada?${app.iniciada}<br>`);
