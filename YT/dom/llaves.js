const contenedor = document.getElementById("container");
let contador = 0;
function crearLlave(nombre,modelo,precio){
	contador++;
img = "<img class='llave-img' src='llave.png'>";
nombre =`<h2 class="texto nombre" >${nombre}</h2>`;
modelo =`<h3 class="texto modelo"  id="${modelo}">${modelo}</h3>`;
precio = `<p class="texto precio" >Precio:<b> $${precio}</b></p>`;
	return [nombre,modelo,precio,img];
}
 const changeHidden = (number) =>{
 	document.querySelector(".key-data").value = number ; 
 }

let docFrag =  document.createDocumentFragment();

for (let i = 1; i <= 20; i++) {
	let modeloRandom = Math.random()*100+1000;
	let precioRandom = Math.random()*10+30;
	modeloRandom = modeloRandom.toFixed();
	precioRandom = precioRandom.toFixed(2);
	let llave = crearLlave(`llave ${i}`, `modelo: ${modeloRandom}`, `${precioRandom}`);
	let div = document.createElement("DIV");
	div.addEventListener("click", (number)=>{changeHidden(modeloRandom)});
	div.tabIndex = i;
	div.classList.add(`flex-item`,`item-${i}`);
	div.innerHTML = llave[3] + llave[0] + llave[1] + llave[2];
	docFrag.appendChild(div);
}
const enviar = document.getElementById("enviar");
contenedor.appendChild(docFrag);




