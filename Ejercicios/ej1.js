let numero = parseFloat(prompt("Introduzca la calificación del 1 al 10"));
if(numero >= 0 && numero < 3){
	alert(`la calificacion: ${numero} es muy deficiente`);
} else if(numero >= 3 && numero < 5){
	alert(`la calificacion: ${numero} es deficiente`);
} else if(numero >= 5 && numero < 6){
	alert(`la calificacion: ${numero} es suficiente`);
} else if(numero >= 6 && numero < 7){
	alert(`la calificacion: ${numero} es buena`);
} else if(numero >= 7 && numero < 9){
	alert(`la calificacion: ${numero} es notable`);
} else if(numero >= 9 && numero <= 10){
	alert(`la calificacion: ${numero} es deficiente`);
} else {
	alert(`Lo sentimos no está dentro del rango`);
}