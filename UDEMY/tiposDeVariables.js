//Tipo de dato variable string
let hola = "Hola";
//tipo de dato number
let num1 = 3.2;
let num2 = 7.5;

document.write(`${typeof hola}<br>`);
//Tipo de dato objeto
let objeto = {
    nombre : "Rene",
    apellido : "Ramirez"
}
document.write(`${typeof objeto}<br>`);
//Tipo de dato booleano
let booleano1 = (true);
let booleano = (hola != objeto);

document.write(`${booleano}<br>`);
//Tipo de dato function
const sumar = (num1, num2)=>{
    return num1 + num2;
}

document.write(`${sumar(num1, num2)}`);
console.log(typeof sumar);

//Tipo de dato Symbol
let simbolo = Symbol("mi simbolo");
console.log(typeof simbolo); 

//Tipo clase es una function

class Persona{
    constructor(nombre, apellido){
        this.nombre = nombre;
        this.apellido = apellido;
    }
}
console.log(typeof Persona);

//Tipo undefined
let x;
console.log(x);

//Null = ausencia de valor

var y = null;

console.log(y);
console.log(typeof y);