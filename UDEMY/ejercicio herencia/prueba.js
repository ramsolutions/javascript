class Persona {
	static contadorPersonas = 0;

	constructor(nombre, apellido, edad){
		this.idPersona = ++Persona.contadorPersonas;
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
	}
	getIdPersona(){
		return this.idPersona;
	}
	getNombre(){
		return this.nombre;
	}
	setNombre(nombre){
		this.nombre =  nombre;
	}
	getApellido(){
		return this.apellido;
	}
	setApellido(apellido){
		this.apellido =  apellido;
	}
	getEdad(){
		return this.edad;
	}
	setEdad(edad){
		this.edad =  edad;
	}	
	toString(){
		return `
		<br>ID: ${this.idPersona}<br>
		Nombre: ${this.nombre}<br>
		Apellido: ${this.apellido}<br>
		Edad: ${this.edad}<br>`;
	}
}
class Empleado extends Persona {
	static contadorEmpleados = 0;
	constructor(nombre, apellido, edad, sueldo){
		super(nombre, apellido, edad);
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.sueldo = sueldo;
		this.idEmpleado = ++Empleado.contadorEmpleados;
	}

	getSueldo(){
		return this.sueldo;
	}
	setSueldo(sueldo){
		this. sueldo = sueldo;
	}

	toString(){
		return`${super.toString()} ID Empleado: ${this.idEmpleado}
		 <br>Sueldo: ${this.sueldo}<br>`
	}
}
class Cliente extends Persona {
	static contadorClientes = 0; 
	constructor(nombre, apellido, edad, fechaRegistro){
		super(nombre, apellido, edad);
		this.edad = edad;
		this.apellido = apellido;
		this.nombre = nombre;
		this.idCliente = ++Cliente.contadorClientes;
		this.fechaRegistro = fechaRegistro;
	}
	getIdCliente(){
		return this.idCliente;
	}
	getFechaRegistro(){
		return this.fechaRegistro;
	}
	setFechaRegistro(fechaRegistro){
		this.fechaRegistro = fechaRegistro;
	}
	toString(){
		return `${super.toString()} ID Cliente: ${this.idCliente}
		<br>Fecha de Registro: ${this.fechaRegistro}<br>`;
	}
}
//Prueba class Persona
let persona1 = new Persona(`Juan`,`Perez`, 25);
document.write(persona1.toString());
console.log(persona1.toString());

let persona2 = new Persona(`Carlos`, `Ramirez`, 35);
console.log(persona2.toString());
document.write(persona2.toString()); 

let empleado1 = new Empleado(`Carla`, `Gomez`, 33);
console.log(empleado1.toString());
document.write(empleado1.toString());

let cliente1 = new Cliente(`Laura`, `Marano`, 45, new Date())
console.log(cliente1.toString());
document.write(cliente1.toString()); 

const crearCliente = ()=>{
	let cliente =  undefined;		
	let nombre = prompt(`proporcione el nombre del cliente`);
	let apellido = prompt(`proporcione el apellido del cliente`);
	let edad = prompt(`proporcione la edad del cliente:`);
	edad = parseInt(edad);
	let fechaRegistro = new Date();
	return cliente = new Cliente(nombre, apellido, edad, fechaRegistro);
}

cliente2 = crearCliente();
cliente3 = crearCliente();

document.write(cliente2.toString());
document.write(cliente3.toString());
