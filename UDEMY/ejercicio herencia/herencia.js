class Persona {
	static contadorPersonas = 0;

	constructor(nombre, apellido, edad){
		this.idPersona = ++Persona.idPersona;
		this.nombre =  nombre;
		this.apellido = apellido;
		this.edad = edad;
	}
	getIdPersona(){
		return this.idPersona;
	}
	getNombre(){
		return this.nombre;
	}
	setNombre(nombre){
		this.nombre =  nombre;
	}
	getApellido(){
		return this.apellido;
	}
	setApellido(apellido){
		this.apellido =  apellido;
	}
	getEdad(){
		return this.edad;
	}
	setEdad(edad){
		this.edad =  edad;
	}	
	toString(){
		return `
		Id: ${this.idPersona}
		Nombre: ${this.nombre}
		Apellido: ${this.apellido}
		Edad: ${this.edad}`;
	}
}
class Empleado extends Persona {
	static contadorEmpleados = 0;
	constructor(sueldo){
		super(this.idPersona, this.nombre, this.apellido, this.edad);
		this.sueldo = sueldo;
		this.idEmpleado = ++Empleado.contadorEmpleados;
	}

	getSueldo(){
		return this.sueldo;
	}
	setSueldo(sueldo){
		this. sueldo = sueldo;
	}

	toString(){
		return`${super.toString()} ID Empleado ${this.idEmpleado}
		 Sueldo: ${this.sueldo}`
	}
}
class Cliente extends Persona {
	static contadorClientes = 0; 
	constructor(fechaRegistro){
		
		this.idCliente =  ++Cliente.contadorClientes;
		this.fechaRegistro = fechaRegistro;
	}
	get idCliente(){
		return this.idCliente;
	}
	get fechaRegistro(){
		return this.fechaRegistro;
	}
	set fechaRegistro(fechaRegistro){
		this.fechaRegistro = fechaRegistro;
	}
	toString(){
		return `${super.toString()} ID Cliente: ${this.idCliente}
		Fecha de Registro: ${this.fechaRegistro}`
	}
}
